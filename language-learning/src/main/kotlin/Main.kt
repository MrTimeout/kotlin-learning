fun main(args: Array<String>) {
    println("Program arguments: ${args.joinToString()}")
}
// Single-line comment
/*
    Multiline comment.
    /* Unlike in Java, multiline comments in kotlin can be nested. */
 */
/**
 * KDoc multiline comments.
 *
 * @param entry is the param of the a() function. We cannot pass a null object because we are not using the question mark "?" after the type.
 * @return a new string in uppercase.
 */
fun a(entry: String): String {
    return entry.uppercase()
    val x = 2; // This is unreachable code in kotlin, however is a compile error in java.
}

fun b(i: Int): Int {
    // Compile error: Parameter are immutable by default.
    // return ++i
    return i
}

// Same as "fun c() {
fun c(): Unit {
    println("Hello world")
}

fun d(a: Int, b: Int): Int = a + b
fun e(a: Int, b: Int) = a + b

fun f(a: Int, b: Int) = { a + b }