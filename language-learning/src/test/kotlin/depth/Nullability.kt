package depth

import com.google.gson.Gson
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import java.io.File
import kotlin.properties.Delegates
import kotlin.reflect.KProperty
import kotlin.test.*

/**
 * - Null safety
 * - Safe call operator (.?)
 * - Elvis operator (?:)
 * - Non-null assertion (!!)
 * - Types: Any, Nothing and Unit
 * - Delegated properties (AWESOME)
 * - Lambdas
 */
internal class Nullability {

    private var content: String by FileStorageProperty(
        "src/test/resources/file.txt",
        WriteMode.APPEND
    )

    companion object {
        private val PIKACHU_INPUT = """
           {
            "name": "Pikachu",
            "naturalElements": ["electric"],
            "weakToElements": ["ground"],
            "image": "https://img.rankedboost.com/wp-content/plugins/pokemon-scarlet-violet/assets/pokemon-images/Pikachu.png"
           }
        """.trimIndent()
        private const val PIKACHU_EXPECTED =
            "name: Pikachu, naturalElement: electric, weakToElements: ground, image: https://img.rankedboost.com/wp-content/plugins/pokemon-scarlet-violet/assets/pokemon-images/Pikachu.png"
    }

    @Test
    fun testGetCountForCharacter() {
        assertEquals(0, getCountForCharacter(null, 's'))
        assertEquals(0, getCountForCharacter("Hello", 'y'))
        assertEquals(1, getCountForCharacter("Hello", 'e'))
    }

    @Test
    fun testNonNullAssertionSuccessfully() {
        val str: String? = "Hello world!"

        assertEquals("HELLO WORLD!", str!!.uppercase())
    }

    @Test
    fun testNonNullAssertionThrowsWhenNull() {
        val str: String? = null

        assertThrows<NullPointerException> { str!!.uppercase() }
    }

    @Test
    fun testAnyType() {
        // Any type has the methods: equals, hashCode and toString
        val first: Any = "Hello world"

        assertEquals(first, generateRandomAny(1))
        assertEquals(2, generateRandomAny(2))
        assertThrows<IllegalArgumentException> { generateRandomAny(3) }
    }

    @Test
    fun testIWillAlwaysFail() {
        assertThrows<IllegalArgumentException> { iWillAlwaysFail() }
    }

    @Test
    fun testPrintSomething() {
        assertIs<Unit>(printSomething())
        assertEquals("kotlin.Unit", printSomething().toString())
    }

    @Test
    fun testDelegatedProperties() {
        val computedOnceExpected = "I am computing this value for a long time"
        val person = Person("Mr", "Timeout")

        // custom delegate property
        assertNotNull(person.fullName)

        // lazy
        assertAll(
            { assertEquals(computedOnceExpected, person.computedOnce) },
            { assertEquals(computedOnceExpected, person.computedOnce) }
        )

        assertTrue(person.variableVar.isEmpty())

        // Delegates.observable
        person.variableVar = "a"
        assertEquals("a", person.variableVar)

        person.variableVar = "b"
        assertEquals("b", person.variableVar)

        // Testing person.age vetoable
        person.age = 10
        assertEquals(10, person.age)

        person.age = Short.MAX_VALUE.toInt()
        assertEquals(10, person.age)

        // Testing person.city vetoable throwing exceptions
        person.city = "Madrid"
        assertEquals("Madrid", person.city)

        assertThrows<IllegalArgumentException> { person.city = "Hello1234Hello" }

        // Delegate to a map. This is awesome
        val pikachuProperties: Map<String, Any> =
            Gson().fromJson<HashMap<String, Any>>(PIKACHU_INPUT, HashMap::class.java)
        val pikachu = Pokemon(pikachuProperties)

        assertEquals(
            PIKACHU_EXPECTED,
            pikachu.toString()
        )

        this.content = "Hello world\n"
        assertEquals("Hello world\n", this.content)

        this.content = "Another Hello world\n"
        assertEquals(
            """
            Hello world
            Another Hello world
            
            """.trimIndent(), this.content
        )
    }

    @Test
    fun testLambdas() {
        // We can invoke lambda functions in this way
        { println("Hello world") }.invoke()
        (({ println("Hello world") })())

        val intSum: (d: Array<Int>) -> Int = { it.sum() }

        assertAll(
            { assertEquals(10, { d: Array<Int> -> d.sum() }.invoke(arrayOf(1, 2, 3, 4))) },
            { assertEquals(10, ({ d: Array<Int> -> d.sum() })(arrayOf(1, 2, 3, 4))) },
            { assertEquals(10, intSum(arrayOf(1, 2, 3, 4))) },
            { assertEquals(10, intSum.invoke(arrayOf(1, 2, 3, 4))) }
        )

        // Awesome syntax
        val countChars: String.(c: Char) -> Int = { c -> this.count { it == c } }

        assertEquals(2, "Hello world".countChars('o'))

    }

    private fun getCountForCharacter(input: String?, characterToSearch: Char): Int =
        input?.count { it == characterToSearch } ?: 0 // This is called the Elvis operator

    private fun generateRandomAny(choose: Int): Any = when (choose) {
        1 -> "Hello world"
        2 -> 2
        else -> {
            throw IllegalArgumentException()
        }
    }

    private fun iWillAlwaysFail(): Nothing = throw IllegalArgumentException()

    private fun printSomething(): Unit = println("Hello world")

    // https://www.baeldung.com/kotlin/delegated-properties
    internal class Person(private val name: String, private val surname: String) {
        var fullName: String by Capitalize()

        // It uses the LazyThreadSafetyMode.SYNCHRONIZED
        val computedOnce: String by lazy {
            "I am computing this value for a long time"
        }

        var variableVar: String by Delegates.observable("") { prop, old, new ->
            println("For the property \"$prop\" the value has changed from \"$old\" to \"$new\"")
        }

        var age: Int by Delegates.vetoable(0) { _, _, new ->
            new in 0..120
        }

        var city: String by Delegates.vetoable("") { _, _, new ->
            if (new.contains("\\d+".toRegex())) throw IllegalArgumentException("Error parsing streetName") else true
        }

        // This example is awesome
        var newProperty: String = ""

        @Deprecated("use newProperty instead of property")
        var property: String by this::newProperty

        init {
            fullName = "${this.name} ${this.surname}"
        }

        private inner class Capitalize {
            operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
                return "the property ${property.name} for the object {$thisRef.toString()}"
            }

            operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String): Unit {
                println("$value was assigned to the ${property.name} in the object ${thisRef.toString()}")
            }
        }
    }

    internal data class Pokemon(private val properties: Map<String, Any>) {
        private val name: String by this.properties
        private val naturalElements: List<String> by this.properties
        private val weakToElements: List<String> by this.properties
        private val image: String by this.properties

        override fun toString(): String =
            "name: ${this.name}, naturalElement: ${this.naturalElements.joinToString(",")}, weakToElements: ${
                this.weakToElements.joinToString(",")
            }, image: ${this.image}"
    }

    internal enum class WriteMode {
        STANDARD,
        APPEND
    }

    // Custom delegate property, this feature is awesome...
    internal class FileStorageProperty(
        private val fileName: String,
        private val writeMode: WriteMode = WriteMode.STANDARD
    ) {

        init {
            File(this.fileName).run {
                if (this.exists() && !this.isFile) {
                    throw IllegalArgumentException("The filename ${this@FileStorageProperty.fileName} is not a file")
                } else if (!this.exists()) {
                    this.createNewFile() || throw IllegalArgumentException("Error Trying to create the file")
                }
            }
        }

        operator fun getValue(thisRef: Any?, property: KProperty<*>): String =
            File(this.fileName).let {
                if (it.exists() && it.isFile) {
                    return it.readText(Charsets.UTF_8)
                }
                throw IllegalArgumentException("Error trying to get the data from the file ${this.fileName}")
            }

        operator fun setValue(thisRef: Any?, property: KProperty<*>, content: String): Unit {
            File(this.fileName).run {
                when (this@FileStorageProperty.writeMode) {
                    WriteMode.STANDARD -> this.writeText(content, Charsets.UTF_8)
                    WriteMode.APPEND -> this.appendText(content, Charsets.UTF_8)
                }
            }
        }
    }
}