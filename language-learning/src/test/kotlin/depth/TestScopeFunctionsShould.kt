package depth

import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

internal class TestScopeFunctionsShould {

    @Test
    internal fun testingLetScopeFunction() {
        val name: String? = null
        val name1: String? = "Hello world!"
        val reversedName1: String = "!dlrow olleH"

        // It asserts the nullability of name using a notNull()
        assertThrows<NullPointerException> { name!!.reversed() }

        // This code won't execute because name is null
        assertEquals(2, name?.let {
            println(it.length)
            println(it.reversed())
        } ?: 2)

        val nameUnit: Unit? = name1?.let {
            assertAll(
                { assertEquals(12, it.length) },
                { assertEquals(reversedName1, it.reversed()) }
            )
        }

        assertEquals("kotlin.Unit", nameUnit.toString())

        println(name1?.let {
            "Hello"
        })
    }

    @Test
    internal fun testingWithScopeFunction() {
        val person = Person("name", "surname", 1u)

        val fullName: String = with(person) {
            assertAll(
                { assertEquals("name", this.name) },
                { assertEquals("surname", this.surname) },
                { assertEquals(1u, this.age) }
            )
            "$name $surname with age of $age"
        }

        assertEquals(fullName, "name surname with age of 1")
    }

    @Test
    internal fun testingRunScopeFunction() {
        val person: Person? = null
        val person0: Person? = Person("name", "surname", 1u)

        person?.run {
            println(this.name)
            println(this.surname)
        }

        val resultOfRun: String? = person0?.run {
            assertAll(
                { assertEquals("name", name) },
                { assertEquals("surname", surname) },
                { assertEquals(1u, age) }
            )
            "hello"
        }

        assertEquals("hello", resultOfRun)
    }

    @Test
    internal fun testingApplyScopeFunction() {
        val car = Car(19).apply {
            model = "piccaso"
            brand = "citroen"
            wheels = 4
        }
        val car1 = Car("piccaso", 4, "citroen").apply {
            age = 19
        }

        with(car) {
            assertAll(
                { assertEquals("piccaso", model) },
                { assertEquals("citroen", brand) },
                { assertEquals(4, wheels) },
                { assertEquals(19, age) }
            )
        }

        with(car1) {
            assertAll(
                { assertEquals("piccaso", model) },
                { assertEquals("citroen", brand) },
                { assertEquals(4, wheels) },
                { assertEquals(19, age) }
            )
        }
    }

    @Test
    internal fun testingAlsoScopeFunction() {
        val list: MutableList<Int> = mutableListOf(1, 2, 3, 4).also {
            assertEquals(
                """
                    - 2
                    - 4""".trimIndent(),
                it.filter { it % 2 == 0 }.map { "- $it" }.joinToString("\n")
            )
        }

        assertContentEquals(listOf(1, 2, 3, 4), list)
    }

    internal data class Person(val name: String, val surname: String, val age: UInt)

    internal class Car(var age: Int) {
        var model: String? = null
        var wheels: Int? = null
        var brand: String? = null

        constructor(model: String, wheels: Int, brand: String) : this(0) {
            this.model = model
            this.wheels = wheels
            this.brand = brand
        }
    }
}