package effective.item1

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.api.assertThrows
import java.util.*
import kotlin.concurrent.thread
import kotlin.streams.toList
import kotlin.test.*

/**
 * The most important features of limiting mutability in kotlin are:
 *
 *  - Read-only properties val
 *  - Separation between mutable and read-only collections
 *  - copy in data classes
 */
internal class LimitMutability {

    @Test
    fun `simple sample of mutability`() {
        var a = 10
        val list: MutableList<Int> = mutableListOf()

        a *= 2
        list.plus(a)

        assertEquals(11, a)
        assertContentEquals(listOf(a), list)
    }

    @Test
    fun `testing BankAccount`() {
        val bankAccount = BankAccount()

        bankAccount.deposit(10.0)
        bankAccount.deposit(10.0)

        assertEquals(20.0, bankAccount.balance)

        bankAccount.withdraw(5.0)

        assertEquals(15.0, bankAccount.balance)

        assertThrows<InsufficientFunds> { bankAccount.withdraw(Double.MAX_VALUE) }
    }

    @Test
    fun `testing BankAccount shared state`() {
        val distinctBankAccounts =
            (1..10).toList().parallelStream().map(this::bankAccountWithThreads)
                .map(BankAccount::balance).distinct().toList()

        // This assertion can fail if the 10 test cases give us the exact result 1000.0.
        assertTrue(
            distinctBankAccounts.size == 1 && distinctBankAccounts[0] != 1000.0 || distinctBankAccounts.size > 1
        )

        val distinctBankAccountWithSync =
            (1..10).toList().parallelStream().map(this::bankAccountWithSync)
                .map(BankAccount::balance).distinct().toList()

        assertTrue(distinctBankAccountWithSync.size == 1 && distinctBankAccountWithSync[0] == 1000.0)
    }

    @Test
    fun `testing BankAccount shared state with coroutines`() {
        val distinctBankAccounts: MutableList<BankAccount> = mutableListOf()

        for (i in 1..10) {
            runBlocking {
                distinctBankAccounts.plus(bankAccountWithCoroutines(i))
            }
        }

        distinctBankAccounts.forEach(::println)

        // It doesn't work
        //assertTrue(
        //distinctBankAccounts.size == 1 && distinctBankAccounts[0].balance != 1000.0 || distinctBankAccounts.size > 1
        //)
    }


    @Test
    fun `testing read-only properties val`() {
        // In kotlin we can make each property read-only val (like value) or read-write var (like variable).
        val a = 10
        // a = 11 // This is not allowed
        // Notice though that read-only properties are not necessarily immutable nor final.

        // A read-only property can hold a mutable object
        val l: MutableList<String> = mutableListOf()

        l.add("Hello")
        l.add(" ")
        l.add("World")
        l.add("!")

        assertEquals("Hello World!", l.joinToString(""))

        class Point(var x: Int, var y: Int) {
            override fun toString(): String = "Point(${this.x}, ${this.y})"
        }

        val p = Point(0, 0)

        p.x = 1
        p.y = 1

        assertEquals("Point(1, 1)", p.toString())

        // A read-only property can also be defined using a custom getter that might depend on another property
        class ReadOnlyPropertyWithCustomGetter(var greeting: String, var name: String) {
            val fullName
                get() = this.formatFullName()
            val fullName1 =
                this.formatFullName() // Here, each time we call this property, its value won't change

            private fun formatFullName() = "${this.greeting} ${this.name}"
        }

        val readOnlyPropertyWithCustomGetter =
            ReadOnlyPropertyWithCustomGetter("Hello", "MrTimeout")

        assertEquals("Hello MrTimeout", readOnlyPropertyWithCustomGetter.fullName)
        assertEquals("Hello MrTimeout", readOnlyPropertyWithCustomGetter.fullName1)

        readOnlyPropertyWithCustomGetter.greeting = "Bye"

        assertEquals("Bye MrTimeout", readOnlyPropertyWithCustomGetter.fullName)
        assertEquals("Hello MrTimeout", readOnlyPropertyWithCustomGetter.fullName1)

        // The core idea is that "val" is only a get() method and "var" is a set() and get().
        // "val" doesn't offer us mutation points.
        // We can also override a "val" property with a "var"
        abstract class Element {
            open val active: Boolean = true
        }

        class ActualElement : Element() {
            override var active: Boolean = false
        }

        // "val" doesn't mean immutable.
        class Person(val name: String?, val surname: String) {
            val fullName: String?
                get() = name?.let { "$it ${this.surname}" }

            val fullName1: String? = name?.let { "$it ${this.surname}" }
        }

        val person = Person("Mr", "Timeout")

        if (person.fullName != null) {
            // We need to test if it is nullable or not because the value can change, because of a custom get
            // Which depends on other properties that are subject to change.
            assertEquals(10, person.fullName?.length)
        }

        if (person.fullName1 != null) {
            // Smart case, because we are using a "static get", which means that the value of fullName1
            // will always be the same.
            assertEquals(10, person.fullName1.length)
        }

    }

    @Test
    fun `Separation between mutable and read-only collections`() {
        val nonMutableList: List<Int> = listOf()

        // plus returns a mutable list (ArrayList) using the interface List, so we have to cast it to ArrayList
        // This is Down-casting collections, we shouldn't use it, because we have to fit the contract of the interface returned.
        assertTrue((nonMutableList.plus(1) as ArrayList<Int>).add(2))

        // If we want to change from nonMutableList to mutableList:
        val mutableList: MutableList<Int> = nonMutableList.plus(1).toMutableList()

        assertTrue(mutableList.size == 1)

        /* Example of iterable
         inline fun<T, R> Iterable<T>.map(transformation: (T) -> R): List<R> {
            val list = ArrayList<R>()
            for (element in this) {
                list.add(transformation(element))
            }
            return list
         }
         */
    }

    @Test
    fun `copy in data classes`() {
        /**
         * Immutable objects have their own advantages:
         * 1. They are easier to reason about since their state stays the same once they are created.
         * 2. Immutability makes it easier to parallelize the program as there are no conflicts among shared objects.
         * 3. References to immutable objects can be cached as they are not going to change.
         * 4. We do not need to make defensive copies on immutable objects. When we do copy immutable objects, we do not need to make it a deep copy.
         * 5. Immutable objects are the perfect material to construct other objects.
         * 6. We can add them to set or use them as keys in maps, in opposition to mutable objects that shouldn't be used this way.
         * This is because both those collections use hash table under the hood in Kotlin/JVM.
         */
        class FullName(var name: String, var surname: String) : Comparable<FullName> {
            override fun compareTo(other: FullName): Int {
                if (this.name != other.name) return if (this.name > other.name) 1 else -1
                if (this.surname != other.surname) return if (this.surname > other.surname) 1 else -1
                return 0
            }

            override fun toString(): String = "name: $name, surname: $surname"
        }

        val names: SortedSet<FullName> = TreeSet()
        val person = FullName("AAA", "AAA")

        names.add(person)
        names.add(FullName("Jordan", "Hansen"))
        names.add(FullName("David", "Blanc"))

        assertAll("person",
            { assertEquals(names.toList()[0], person) },
            { assertTrue(person in names) }
        )

        person.name = "ZZZ"

        assertAll("person",
            // SortedSet is not reordered when changing the value of a reference in the set
            { assertEquals(names.toList()[0], person) },
            // It is not possible to find the person object because it is not ordered correctly
            { assertFalse(person in names) }
        )

        assertAll("person",
            { assertEquals(names.toSortedSet().toList()[2], person) },
            { assertTrue(person in names.toSortedSet()) }
        )

        // Creating immutable objects like Int, Iterable, etc.
        class User(val name: String, val surname: String) {
            fun withSurname(surname: String) = User(this.name, surname)

            fun withName(name: String) = User(name, this.surname)
        }

        val u1 = User("name", "surname")
        val u2 = u1.withName("name")
        val u3 = u1.withSurname("surname")

        assertNotEquals(u1, u2)
        assertNotEquals(u1, u3)

        // The previous solution is just awesome, but we need another one, so we don't write so much code
        // This is an elegant and universal solution that supports making data model classes immutable.
        data class UserData(val name: String, val surname: String)

        val ud1 = UserData("name", "surname")
        val ud2 = ud1.copy(surname = "surname")
        val ud3 = ud1.copy(name = "name")

        assertEquals(ud1, ud2)
        assertEquals(ud1, ud3)
    }

    private fun bankAccountWithThreads(step: Int): BankAccount {
        println("Running step $step")
        val bankAccount = BankAccount()

        // Using shared mutable state, when using threads we are losing some actions.
        // Using coroutines we lose less, because less threads are involved.
        for (i in 1..1000) {
            thread {
                Thread.sleep(10)
                bankAccount.deposit(1.0)
            }
        }
        Thread.sleep(5000)
        return bankAccount
    }

    private fun bankAccountWithSync(index: Int): BankAccount {
        println("Running step $index")
        // We can use any object to synchronize lock
        val lock = Any()
        val bankAccount = BankAccount()

        for (i in 1..1000) {
            thread {
                Thread.sleep(10)
                synchronized(lock) {
                    bankAccount.deposit(1.0)
                }
            }
        }

        Thread.sleep(1000)
        return bankAccount
    }

    private suspend fun bankAccountWithCoroutines(step: Int): BankAccount {
        println("Running step $step")
        val bankAccount = BankAccount()
        coroutineScope {
            for (i in 1..1000) {
                launch {
                    delay(10)
                    bankAccount.deposit(1.0)
                }
            }
        }
        return bankAccount
    }
}

/**
 * Here BankAccount has a state that represents how much money is present on that account.
 *
 */
internal class BankAccount {
    var balance = 0.0
        private set

    fun deposit(depositAmount: Double) {
        this.balance += depositAmount
    }

    @Throws(InsufficientFunds::class)
    fun withdraw(withdrawAmount: Double) {
        if (this.balance < withdrawAmount) {
            throw InsufficientFunds()
        }
        balance -= withdrawAmount
    }

    override fun toString(): String = "BankAccount balance = ${this.balance}"
}

internal class BankAccountWithoutStateMutability() {
    private var balance = 0.0

    private constructor(balance: Double) : this() {
        this.balance = balance
    }

    fun deposit(depositAmount: Double): BankAccountWithoutStateMutability {
        return BankAccountWithoutStateMutability(this.balance + depositAmount)
    }

    @Throws(InsufficientFunds::class)
    fun withdraw(withdrawAmount: Double): BankAccountWithoutStateMutability {
        if (this.balance < withdrawAmount) {
            throw InsufficientFunds()
        }
        return BankAccountWithoutStateMutability(this.balance - withdrawAmount)
    }
}

internal class InsufficientFunds : Exception()