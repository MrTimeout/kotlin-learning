package files.xml

import org.w3c.dom.Document
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import java.io.File
import java.io.StringReader
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import kotlin.test.Test

internal class TestXmlShould {

    companion object {
        private const val XML_FILE = "src/test/resources/files/xml/file.xml"
    }

    @Test
    fun shouldReadXmlFileCorrectly() {
        val xmlReader = XmlReader(XML_FILE)

        val nodeList = xmlReader.xPathLook("/teachers/teacher") as NodeList

        (0 until nodeList.length).map(nodeList::item).map{ it.childNodes.item(1).nodeName }.forEach(::println)
    }

    internal class XmlReader(private val pathFile: String) {
        private val documentBuilderFactory: DocumentBuilderFactory by lazy { DocumentBuilderFactory.newInstance() }

        private val documentBuilder: DocumentBuilder get() = this.documentBuilderFactory.newDocumentBuilder()

        private val xPathFactory: XPathFactory by lazy { XPathFactory.newInstance() }

        private val xPath: XPath get() = this.xPathFactory.newXPath()

        private val document = this.parseDocument(this.inputSource())

        fun xPathLook(xpath: String): Any =
            this.xPath.evaluate(xpath, this.document, XPathConstants.NODESET)

        private fun parseDocument(inputSource: InputSource): Document =
            this.documentBuilder.parse(inputSource)

        private fun inputSource(): InputSource =
            InputSource(StringReader(File(this.pathFile).readText()))
    }
}