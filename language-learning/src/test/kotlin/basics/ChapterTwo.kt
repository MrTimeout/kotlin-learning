package basics

import org.junit.jupiter.api.assertThrows
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

internal class ChapterTwo {

    @Test
    fun testNPE() {
        val input: String? = null

        // Here, intellij knows that it is always going to fail.
        assertThrows<NullPointerException> { input!!.toInt() }
    }

    @Test
    fun testNumericFormatException() {
        val input = "\n"

        assertThrows<NumberFormatException> { input.toInt() }
    }

    @Test
    fun testVariables() {
        val a: String
        val b = "Hello world" // This is called "Type inference" because kotlin knows what type is the variable. We can know it using intellij Ctrl+Shift+P
        var c = "Mutable variable"
        var d: String
        val e = "Hello world"
        //println(a) // Uncommenting this line will throw an error, because we are reading a variable that hasn't already initialized.
        // a = "Hello world" // When declaring this way, being the value assigned a String literal, Intellij give us a suggestion of doing this statement one liner.
        a = b.uppercase() // When declaring this way, all is fine.
        // a = "other stuff" // val cannot be reassigned. We have to use var keyword, so it is not immutable.
        // Until now, we haven't modified the value of c variable, and we are reading it here. Kotlin suggests to use the "c" variable as val, instead of val because we are not modifying its value before the first read.
        assertEquals("HELLO WORLD", a)
        assertEquals("Hello world", b)
        assertEquals("Mutable variable", c)

        c = "another value" // If we modify here the value of the "c" variable, kotlin stops suggesting to transform the "c" variable to immutable.
        assertEquals("another value", c)

        // d = "heeey" // If we uncomment this code, kotlin will propose us to use one liner initialization and with the second line, kotlin will suggest to use the second value instead of the first one.
        d = e.uppercase()
        assertEquals("HELLO WORLD", d)

        d = "other"
        assertEquals("other", d)
    }

    @Test
    fun `identifiers in kotlin`() {
        // It may only contain letters, digits and underscore characters and may not start with a digit.
        // _, __, ___ and so on are reserved and can't be used as identifiers.
        // ([basics.A-Za-z][basics.A-Za-z_]+[0-9]*)+ // not tested
        // Letters and Digits, like in Java, are not limited to ASCII, but include national alphanumeric characters as well.
        // unlike in java, dollar sign ($) is not allowed in kotlin identifiers
    }

    @Test
    fun `mutable variables`() {
        var sum = 1

        sum += 1
        assertEquals(2, sum)

        sum -= 1
        assertEquals(1, sum)

        sum *= 4
        assertEquals(4, sum)

        sum /= 2
        assertEquals(2, sum)

        sum %= 3
        assertEquals(2, sum)

        // First, ++sum => sum = sum + 1 and then asserts.
        assertEquals(3, ++sum)

        assertEquals(3, sum++)

        assertEquals(3, --sum)

        assertEquals(3, sum--)

        assertEquals(2, sum)

        // Expressions not allowed in kotlin
        // Java: a = b = c // Kotlin assignments are statements, rather than expressions and do not return any value.
        /* Kotlin code
            var a: String
            val b = "Hello"
            println(a = b) // In Kotlin, this doesn't work because "a = b" is a statement and not an expression.
        */
        // Same example in Java
        // int a; final int b = 2; System.out.println(a = b); // It will output 2 and will store this value in a variable.
    }

    @Test
    fun stringTesting() {
        // Basic String instantiation
        val hello = "Hello world"

        assertEquals("Hello world", hello)

        val piRepresentation = "\u0eC0 \u2248 3.1415"

        println(piRepresentation)

        // This is called String template
        val embeddedString = "Listen to me, $hello"

        assertEquals("Listen to me, Hello world", embeddedString)

        // Multiline strings
        val multiline = """
            This
            is
            my
            multiline
            String
        """.trimIndent()

        assertEquals(4, multiline.count { it == '\n' })

        val multilineWithTemplate = """
            This
            is
            my
            greeting:
            $hello
            ${"\"\"\""}
        """.trimIndent()

        assertEquals(4, multilineWithTemplate.split('\n').indexOfFirst { it == hello })
        assertEquals("\"\"\"", multilineWithTemplate.split('\n').last())
    }

    @Test
    fun testingArr() {
        val arr: IntArray = intArrayOf(0, 1, 2, 3, 4)

        assertContentEquals(intArrayOf(0, 1, 2, 3, 4), arr)

        increment(arr)

        assertContentEquals(intArrayOf(1, 2, 3, 4, 5), arr)
        assertEquals("[1, 2, 3, 4, 5]", arr.contentToString())
    }

    @Test
    fun funWithNamedArguments() {
        assertContentEquals(listOf(1, 2), range(0, 1))

        assertContentEquals(listOf(4, 5), range(3, to = 4))

        assertContentEquals(listOf(2, 3), range(from = 1, to = 2))
    }

    @Test
    fun testingVarArgs() {
        assertEquals(10, sum(*intArrayOf(1, 2, 3, 4)))

        assertEquals(10, sum(1, 2, 3, 4))

        assertEquals(10, sum(1, 2, *intArrayOf(3, 4)))

        assertEquals("Sum: 10", sumWithPrefix(entry = *intArrayOf(1, 2, 3, 4)))
        assertEquals("Here: 10", sumWithPrefix(prefix = "Here:",1, 2, 3, 4))
    }

    private fun increment(a: IntArray) {
        a.forEachIndexed { i, _ ->
            a[i]++
        }
    }

    private fun range(from: Int, to: Int): List<Int> {
        return (from..to).map { it + 1 }
    }

    private fun sum(vararg entry: Int): Int = entry.reduce { acc, i -> acc + i }

    private fun sumWithPrefix(prefix: String = "Sum:", vararg entry: Int): String = "$prefix ${sum(*entry)}"
}