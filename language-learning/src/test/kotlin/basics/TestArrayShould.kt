package basics

import kotlin.test.*

internal class TestArrayShould {

    @Test
    fun `Initializing Arrays`() {
        val emptyArrayString: Array<String> = emptyArray()
        val fillArrayString = arrayOf("Hello", "world", "!")
        val nullArray: Array<String?> = arrayOfNulls(10)

        assertTrue(emptyArrayString.isEmpty())
        assertFalse(fillArrayString.isEmpty())
        assertTrue(nullArray.size == 10)
        assertTrue(nullArray.all { it.isNullOrEmpty() }) // Awesome, the value is null, and we can just check if it is null or not by its type.

        // Array by type

        // Byte arrays (8 bits - 1 Byte)
        val byteArray: ByteArray = byteArrayOf(-0b10000000, 0b1111111)
        val byteArray1: Array<Byte> = arrayOf(-128, 127)

        assertEquals(byteArray1.contentToString(), byteArray.contentToString())

        // UByte arrays
        @OptIn(ExperimentalUnsignedTypes::class)
        val uByteArray: UByteArray = ubyteArrayOf(0b00000000u, 0b10000000u, 0b11111111u)
        val uByteArray1: Array<UByte> = arrayOf(0u, 128u, 255u)

        assertEquals(
            uByteArray1.contentToString(),
            @OptIn(ExperimentalUnsignedTypes::class) uByteArray.contentToString()
        )

        // Short arrays (16 bits - 2 Bytes)
        val shortArray: ShortArray = shortArrayOf(0x7FFF, -0x8000)
        val shortArray1: Array<Short> = arrayOf(Short.MAX_VALUE, Short.MIN_VALUE)

        assertEquals(shortArray1.contentToString(), shortArray.contentToString())

        // UShort arrays
        @OptIn(ExperimentalUnsignedTypes::class)
        val uShortArray: UShortArray = ushortArrayOf(0x0000u, 0xFFFFu)
        val uShortArray1: Array<UShort> = arrayOf(0u, UShort.MAX_VALUE)

        assertEquals(
            uShortArray1.contentToString(),
            @OptIn(ExperimentalUnsignedTypes::class) uShortArray.contentToString()
        )

        // Int arrays (32 bits - 4 Bytes)
        val intArray: IntArray = intArrayOf(0x7FFFFFFF, -0x80000000)
        val intArray1: Array<Int> = arrayOf(Int.MAX_VALUE, Int.MIN_VALUE)

        assertEquals(intArray1.contentToString(), intArray.contentToString())

        // UInt arrays
        @OptIn(ExperimentalUnsignedTypes::class)
        val uIntArray: UIntArray = uintArrayOf(0x0000000u, 0xFFFFFFFFu)
        val uIntArray1: Array<UInt> = arrayOf(0u, UInt.MAX_VALUE)

        assertEquals(
            uIntArray1.contentToString(),
            @OptIn(ExperimentalUnsignedTypes::class) uIntArray.contentToString()
        )

        // Long arrays (64 bits - 8 Bytes)
        val longArray: LongArray = longArrayOf(0x7FFFFFFFFFFFFFFFL)
        val longArray1: Array<Long> = arrayOf(Long.MAX_VALUE)

        assertEquals(
            longArray1.contentToString(),
            longArray.contentToString()
        )

        // ULong arrays
        @OptIn(ExperimentalUnsignedTypes::class)
        val uLongArray: ULongArray = ulongArrayOf(0x000000000000000uL, 0xFFFFFFFFFFFFFFFFuL)
        val uLongArray1: Array<ULong> = arrayOf(ULong.MIN_VALUE, ULong.MAX_VALUE)

        assertEquals(
            uLongArray1.contentToString(),
            @OptIn(ExperimentalUnsignedTypes::class) uLongArray.contentToString()
        )
    }

    @Test
    fun `Array plus`() {
        val emptyStringArray: Array<String> = emptyArray()

        // plus and plusElement don't modify the array.
        assertContentEquals(arrayOf("Hello"), emptyStringArray.plus("Hello"))
        assertContentEquals(
            arrayOf("Hello", "World"),
            emptyStringArray.plus(arrayOf("Hello", "World"))
        )
        assertContentEquals(
            arrayOf("Hello", "World", "!"),
            emptyStringArray.plus(listOf("Hello", "World", "!"))
        )
        assertContentEquals(arrayOf("Hello"), emptyStringArray.plus(setOf("Hello", "Hello")))
        assertContentEquals(arrayOf("Bye"), emptyStringArray.plusElement("Bye"))

        assertContentEquals(emptyArray(), emptyStringArray)
    }

    @Test
    fun `testing filter`() {
        val expectedNumbers: List<Int> = listOf(2, 4)
        val numbers: Array<Int> = arrayOf(1, 2, 3, 4)

        // filter
        assertContentEquals(expectedNumbers, numbers.filter { it % 2 == 0 })

        // filterTo
        val numbersDestination = mutableListOf<Int>()
        assertContentEquals(expectedNumbers, numbers.filterTo(numbersDestination) { it % 2 == 0 })
        assertContentEquals(expectedNumbers, numbersDestination)
        numbersDestination.clear()

        // filterIndexed. Here we are filtering by even index
        assertContentEquals(listOf(1, 3), numbers.filterIndexed { i, _ -> i % 2 == 0 })

        // filterIndexedTo
        numbers.filterIndexedTo(numbersDestination) { i, _ -> i % 2 == 0 }
        numbersDestination.clear()

        // filterNot
        assertContentEquals(expectedNumbers, numbers.filterNot { it % 2 != 0 })

        // filterNotTo
        assertContentEquals(
            expectedNumbers,
            numbers.filterNotTo(numbersDestination) { it % 2 != 0 })
        assertContentEquals(expectedNumbers, numbersDestination)
        numbersDestination.clear()

        // filterNotNull
        assertContentEquals(
            expectedNumbers,
            arrayOf(1, null, 2, null, 3, null, 4).filterNotNull().filter { it % 2 == 0 }
        )

        // filterNotNullTo
        assertContentEquals(
            expectedNumbers,
            arrayOf(2, null, null, 4).filterNotNullTo(numbersDestination)
        )
        assertContentEquals(expectedNumbers, numbersDestination)
        numbersDestination.clear()

        // filterIsInstance
        assertContentEquals(
            expectedNumbers, arrayOf(2, 2.5f, 2.7, 2L, 4).filterIsInstance<Int>()
        )

        // filterIsInstanceTo
        assertContentEquals(
            expectedNumbers,
            arrayOf(2, 2.5f, 2.7, 2L, 4).filterIsInstanceTo(numbersDestination)
        )
        assertContentEquals(expectedNumbers, numbersDestination)
        numbersDestination.clear()
    }

    @Test
    fun `testing map`() {
        val expectedNumbers: List<Int> = listOf(2, 4, 6, 8)
        val numbers: Array<Int> = arrayOf(1, 2, 3, 4)

        // map
        assertContentEquals(expectedNumbers, numbers.map { it * 2 })

        // mapTo
        val numbersDestination: MutableCollection<Int> = mutableListOf();
        assertContentEquals(expectedNumbers, numbers.mapTo(numbersDestination) { it * 2 })
        assertContentEquals(expectedNumbers, numbersDestination)
        numbersDestination.clear()

        // mapIndexed
        val arrayWithIndex: List<Int> = listOf(0, 2, 6, 12)
        assertContentEquals(arrayWithIndex, numbers.mapIndexed { i, v -> i * v })

        // mapIndexedTo
        assertContentEquals(
            arrayWithIndex,
            numbers.mapIndexedTo(numbersDestination) { i, v -> i * v }
        )
        assertContentEquals(arrayWithIndex, numbersDestination)

        // mapNotNull
        val arrayWithNulls: Array<Int?> = arrayOf(1, 2, 3, 4, null, null, null, null)
        assertContentEquals(expectedNumbers, arrayWithNulls.mapNotNull { it?.times(2) })
        assertContentEquals(expectedNumbers, arrayWithNulls.filterNotNull().map { it * 2 })

        // mapIndexedNotNull
        assertContentEquals(
            arrayWithIndex,
            arrayWithNulls.mapIndexedNotNull { i, v -> v?.times(i) })
        assertContentEquals(
            arrayWithIndex,
            arrayWithNulls.filterNotNull().mapIndexed { i, v -> v.times(i) }
        )
    }
}