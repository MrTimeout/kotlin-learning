package basics

import org.junit.jupiter.api.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

internal class TestClassShould {

    companion object {
        private const val FIRST_PROPERTY = "first"

        private const val SECOND_PROPERTY = "second"

        private const val TESTING_PROPERTIES_EXPECTED = "\"first\" and \"second\""
    }

    @Test
    fun `testing A`() {
        val a = A()
        a.firstProperty = FIRST_PROPERTY
        a.secondProperty = SECOND_PROPERTY

        assertEquals(TESTING_PROPERTIES_EXPECTED, a.printMyProperties())
    }

    @Test
    fun `testing B`() {
        val b = B(FIRST_PROPERTY, SECOND_PROPERTY)

        assertEquals(TESTING_PROPERTIES_EXPECTED, b.finalMessage)
    }

    @Test
    fun `testing ComposeOfB`() {
        val composeOfB =
            ComposeOfB(B(FIRST_PROPERTY, SECOND_PROPERTY), B(FIRST_PROPERTY, SECOND_PROPERTY))

        assertEquals(
            TESTING_PROPERTIES_EXPECTED + "\n" + TESTING_PROPERTIES_EXPECTED,
            composeOfB.finalMessage
        )
    }

    @Test
    fun `testing C`() {
        val firstConstructor = C(FIRST_PROPERTY, SECOND_PROPERTY)
        val secondConstructor = C(FIRST_PROPERTY)

        assertEquals(FIRST_PROPERTY, firstConstructor.firstProperty)
        assertEquals(SECOND_PROPERTY, firstConstructor.secondProperty)
        assertEquals(FIRST_PROPERTY.uppercase(), secondConstructor.secondProperty)
    }

    @Test
    fun `testing D`() {
        val secondConstructor = D(FIRST_PROPERTY)

        assertEquals(FIRST_PROPERTY, secondConstructor.firstProperty)
        assertEquals(FIRST_PROPERTY.uppercase(), secondConstructor.secondProperty)
    }

    @Test
    fun `testing E`() {
        val constructingE = E(FIRST_PROPERTY, SECOND_PROPERTY)

        // We can't access the attributes of class basics.E because they are private
        // assertEquals(FIRST_PROPERTY, constructingE.firstProperty)
        // assertEquals(SECOND_PROPERTY, constructingE.secondProperty)
        assertEquals(TESTING_PROPERTIES_EXPECTED.replace("\"", ""), constructingE.fullProperties())
    }

    @Test
    fun `testing F`() {
        // val constructingF = basics.F() // the constructor is private, so we can't instantiate the class
    }

    @Test
    fun `testing OuterClass`() {
        val outerClass = OuterClass(FIRST_PROPERTY, SECOND_PROPERTY)
        val innerClass = outerClass.InnerClass(FIRST_PROPERTY, SECOND_PROPERTY)
        val anotherInnerClass = outerClass.AnotherInnerClass(FIRST_PROPERTY, SECOND_PROPERTY)
        val staticClass = OuterClass.StaticClass(FIRST_PROPERTY, SECOND_PROPERTY)

        assertEquals(TESTING_PROPERTIES_EXPECTED, outerClass.showMyProperties())

        // Accessing basics.OuterClass without using the this keyword
        assertEquals(TESTING_PROPERTIES_EXPECTED, innerClass.displayPropertiesOfOuterClass())
        assertEquals(TESTING_PROPERTIES_EXPECTED, anotherInnerClass.displayPropertiesOfOuterClass())

        // Using this@basics.OuterClass from inner class
        assertEquals(TESTING_PROPERTIES_EXPECTED, innerClass.accessingOuterClass())
        assertEquals(TESTING_PROPERTIES_EXPECTED, anotherInnerClass.accessingOuterClass())

        // Static classes
        assertEquals(TESTING_PROPERTIES_EXPECTED, staticClass.info())

        assertEquals(TESTING_PROPERTIES_EXPECTED, outerClass.showInnerInfo(innerClass))
        assertEquals(TESTING_PROPERTIES_EXPECTED, outerClass.showInnerInfo(anotherInnerClass))
        assertThrows<IllegalArgumentException> { outerClass.showInnerInfo(outerClass.DoNotUseMe()) }
    }

    @Test
    fun `testing classes inside fun`() {
        val pointResult: String = point()

        assertEquals("Point(0, 0)", pointResult)

        val incrementResult: Int = scopeOfFunFromClassInsideFun()

        assertEquals(3, incrementResult)
    }

    @Test
    fun `testing late init`() {
        val lateInitSample = LateinitSample()
        val lateInitSampleWithoutLateInit = LateinitSampleWithoutLateInit()

        lateInitSample.loadValue(FIRST_PROPERTY)
        lateInitSampleWithoutLateInit.loadValue(FIRST_PROPERTY)

        assertEquals(FIRST_PROPERTY.uppercase(), lateInitSample.value)
        assertEquals(FIRST_PROPERTY.uppercase(), lateInitSampleWithoutLateInit.value)

        lateInitSample.loadValue(null)
        lateInitSampleWithoutLateInit.loadValue(null)

        assertEquals("default", lateInitSample.value)
        assertEquals("default", lateInitSampleWithoutLateInit.value)
    }

    @Test
    fun `testing getters and setters`() {
        val person = Person("Mr", "Timeout")
    }
}

internal class A {
    // Class properties must have a default value when not constructor is present.
    // Class visibility is public by default in kotlin, unlike java, which is limited to the containing package.
    // In kotlin, we can declare more than one public class in a single file, as opposed to java.
    var firstProperty: String = ""
    var secondProperty: String = ""

    fun printMyProperties() = "\"$firstProperty\" and \"$secondProperty\""
}

// Primary constructor declaration
internal class B(firstProperty: String, secondProperty: String = "") {
    val finalMessage = "\"$firstProperty\" and \"$secondProperty\""

    init {
        println(finalMessage)
    }
}

internal class ComposeOfB(vararg b: B) {
    val finalMessage = b.map(B::finalMessage).joinToString("\n")

    init {
        println(finalMessage)
    }
}

internal class C {
    val firstProperty: String
    val secondProperty: String

    // If class doesn't have a primary constructor, then every secondary constructor invokes property
    // initializers and init blocks before executing its own body.
    init {
        println("Hello world from init phase")
    }

    constructor(firstProperty: String, secondProperty: String) {
        println("Running inside secondary constructor")
        this.firstProperty = firstProperty
        this.secondProperty = secondProperty
    }

    // Instead of using optional parameter, we can create another secondary constructor and set the value manually
    constructor(firstProperty: String) {
        println("Running inside secondary constructor")
        this.firstProperty = firstProperty
        this.secondProperty = this.firstProperty.uppercase()
    }
}

internal class D {
    val firstProperty: String
    val secondProperty: String

    constructor(firstProperty: String, secondProperty: String) {
        this.firstProperty = firstProperty
        this.secondProperty = secondProperty
    }

    // We can call another secondary constructor to avoid boilerplate code
    constructor(firstProperty: String) : this(firstProperty, firstProperty.uppercase())
}

internal class E(private val firstProperty: String, private val secondProperty: String) {
    fun fullProperties() = "$firstProperty and $secondProperty"
}

internal class F private constructor() {
    fun showMe() = println("Hello")
}

internal class OuterClass(private val first: String, private val second: String) {
    open inner class PrimitiveInnerClass {
        fun displayPropertiesOfOuterClass() = showMyProperties()
        fun accessingOuterClass() = this@OuterClass.showMyProperties()
    }

    inner class InnerClass(private val first: String, private val second: String) :
        PrimitiveInnerClass() {
        fun innerClassFun() = "\"$first\" and \"$second\""
    }

    inner class AnotherInnerClass(private val first: String, private val second: String) :
        PrimitiveInnerClass() {
        fun anotherInnerClassFun() = "\"$first\" and \"$second\""
    }

    inner class DoNotUseMe : PrimitiveInnerClass()

    open class PrimitiveStaticClass

    // This is equivalent to: static class StaticClass, so we can't extend from an inner class, because it belongs to the instance and not to the class.
    class StaticClass(private val first: String, private val second: String) :
        PrimitiveStaticClass() {
        fun info() = "\"$first\" and \"$second\""
    }

    fun showInnerInfo(which: PrimitiveInnerClass) = when (which) {
        is InnerClass -> which.innerClassFun()
        is AnotherInnerClass -> which.anotherInnerClassFun()
        else -> throw IllegalArgumentException()
    }

    fun showMyProperties() = "\"$first\" and \"$second\""
}

internal fun point(): String {
    class Point(val x: Int, val y: Int) {
        override fun toString() = "Point($x, $y)"
    }

    return Point(0, 0).toString()
}

internal fun scopeOfFunFromClassInsideFun(): Int {
    var x = 1

    class Counter {
        fun increment() {
            // Unlike kotlin, Java doesn't allow modification of captured variables.
            x++
        }
    }

    Counter().increment()
    Counter().increment()

    return x
}

internal class LateinitSampleWithoutLateInit {
    internal var value: String? = null

    fun loadValue(entry: String?) {
        this.value = entry?.uppercase() ?: "default"
    }
}

internal class LateinitSample {
    internal lateinit var value: String

    fun loadValue(entry: String?) {
        this.value = entry?.uppercase() ?: "default"
    }
}

internal class Person(private var name: String, private var surname: String) {

}

/**
 * # Member visibility
 *
 * - public: default. basics.A member may be used anywhere
 * - internal: basics.A member is accessible only within the compilation module containing its class.
 * - protected: basics.A member is accessible within the containing class and all of its subclasses.
 * - private: basics.A member is accessible only within the containing class body.
 */