package basics

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

internal class TestNullabilityShould {

    @Test
    fun testingSafeCall() {
        assertNull(safeCallOperator(null))
        assertEquals("HELLO", safeCallOperator("hello"))

        assertEquals("default", elvisOperator(null))
        assertEquals("HELLO", elvisOperator("hello"))
    }

    private fun safeCallOperator(entry: String?): String? {
        // This is the same as:
        // return if (entry != null) entry.uppercase() else null
        return entry?.uppercase()
    }

    private fun elvisOperator(entry: String?): String {
        return entry?.uppercase() ?: "default"
    }
}