package com.learning.mvvm.doglistretrofit

import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView.OnQueryTextListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.learning.mvvm.doglistretrofit.databinding.ActivityMainBinding
import com.learning.mvvm.doglistretrofit.service.ApiDogService
import com.learning.mvvm.doglistretrofit.viewmodel.DogAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity(), OnQueryTextListener {

    companion object {
        private const val DOGS_API_URL = "https://dog.ceo/api/"
    }

    private lateinit var binding: ActivityMainBinding

    private lateinit var dogAdapter: DogAdapter

    private val dogImages = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        this.binding.svDogs.setOnQueryTextListener(this)
        initRecycleView()
    }

    private fun initRecycleView() {
        this.dogAdapter = DogAdapter(this.dogImages)
        this.binding.rvDogs.layoutManager = LinearLayoutManager(this)
        this.binding.rvDogs.adapter = this.dogAdapter
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(DOGS_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun fetchDogsByName(name: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = getRetrofit().create(ApiDogService::class.java).getDogsByBreed(name)
            runOnUiThread {
                if (response.isSuccessful) {
                    val puppies = response.body()?.images ?: emptyList()
                    dogImages.clear()
                    dogImages.addAll(puppies)
                    dogAdapter.notifyDataSetChanged()
                    hideKeyboard()
                } else {
                    showError()
                }
            }
        }
    }

    private fun hideKeyboard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.binding.viewRoot.windowToken, 0)
    }

    private fun showError() {
        Toast.makeText(this, "An error has occurred", Toast.LENGTH_SHORT).show()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            this.fetchDogsByName(query.lowercase())
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean = true
}