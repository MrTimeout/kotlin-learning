package com.learning.mvvm.doglistretrofit.model

import com.google.gson.annotations.SerializedName

data class BreedResponse(
    @SerializedName("message") val breeds: HashMap<String, Set<String>>,
    @SerializedName("status") val status: String
)

data class SubBreedResponse(
    @SerializedName("message") val subBreeds: Set<String>,
    @SerializedName("status") val status: String
)

data class SubBreedImagesResponse(
    @SerializedName("message") val imagesBySubBreed: Set<String>,
    @SerializedName("status") val status: String
)

data class DogResponse(
    @SerializedName("message") val images: Set<String>,
    @SerializedName("status") val status: String
)
