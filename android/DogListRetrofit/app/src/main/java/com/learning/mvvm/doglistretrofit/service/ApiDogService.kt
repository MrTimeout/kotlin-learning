package com.learning.mvvm.doglistretrofit.service

import com.learning.mvvm.doglistretrofit.model.BreedResponse
import com.learning.mvvm.doglistretrofit.model.DogResponse
import com.learning.mvvm.doglistretrofit.model.SubBreedImagesResponse
import com.learning.mvvm.doglistretrofit.model.SubBreedResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiDogService {
    @GET("breed/{breed}/images")
    suspend fun getDogsByBreed(@Path("breed") breed: String): Response<DogResponse>

    @GET("breeds/list/all")
    suspend fun getBreeds(): Response<BreedResponse>

    @GET("breed/{breed}/list")
    suspend fun getSubBreedsByBreed(@Path("breed") breed: String): Response<SubBreedResponse>

    @GET("breed/{breed}/{subBreed}/images/random/{amount}")
    suspend fun getSubBreedsImages(
        @Path("breed") breed: String,
        @Path("subBreed") subBreed: String,
        @Path("amount") amount: UInt
    ): Response<SubBreedImagesResponse>
}