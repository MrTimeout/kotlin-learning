package com.learning.mvvm.doglistretrofit.viewmodel

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.learning.mvvm.doglistretrofit.databinding.ItemDogBinding
import com.squareup.picasso.Picasso

class DogViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemDogBinding.bind(view)

    fun render(imageUrl: String) {
        Picasso.get().load(imageUrl).into(binding.ivDog)
    }
}