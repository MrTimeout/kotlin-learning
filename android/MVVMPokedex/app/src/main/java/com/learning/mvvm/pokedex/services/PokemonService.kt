package com.learning.mvvm.pokedex.services

import com.learning.mvvm.pokedex.model.PokemonResponse
import retrofit2.Response
import retrofit2.http.GET

interface PokemonService {
    @GET("")
    suspend fun getPokemonList(): Response<PokemonResponse>
}