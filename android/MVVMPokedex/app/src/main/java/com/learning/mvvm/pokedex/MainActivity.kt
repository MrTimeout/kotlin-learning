package com.learning.mvvm.pokedex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

/**
 * An Activity is each of the windows that
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}