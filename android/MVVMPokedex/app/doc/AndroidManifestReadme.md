# Android Manifest

## android:allowBackup

Whether to allow the application to participate in the backup and restore
infrastructure.

### Values

- default value is `true`

## android:supportsRtl

Declares whether your application is willing to support right-to-left(RTL) layouts.

### Values

- `true` and `targetSdkVersion` targets 17 or higher.
- `false` and `targetSdkVersion` is set to 16 or lower.
- default value is set to `false`

## android:testOnly

Indicates whether this application is only for testing purposes.

This kind of APK can be installed only through _adb_ - you cannot publish it to Google Play.

## android:usesCleartextTraffic

Indicates whether the app intends to use cleartext network traffic.

### Values

- APILevel 27 or lower is `true` by default.
- APILevel 28 or higher default to `false`.

## Resources

- [application](https://developer.android.com/guide/topics/manifest/application-element)