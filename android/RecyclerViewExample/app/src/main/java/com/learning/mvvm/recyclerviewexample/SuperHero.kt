package com.learning.mvvm.recyclerviewexample

data class SuperHero(
    val name: String,
    val realName: String,
    val publisher: PublisherEnum,
    val photo: String
)

enum class PublisherEnum(val value: String) {
    DC("DC"),
    MARVEL("Marvel")
}