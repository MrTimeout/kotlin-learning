package com.learning.mvvm.recyclerviewexample

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.learning.mvvm.recyclerviewexample.adapter.SuperHeroAdapter
import com.learning.mvvm.recyclerviewexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val manager = LinearLayoutManager(this)
        // val decoration = DividerItemDecoration(this, manager.orientation)
        binding.recyclerview.layoutManager = manager
        binding.recyclerview.adapter =
            SuperHeroAdapter(SuperHeroProvider.superHeroList) { superHero ->
                this.onItemSelected(
                    superHero
                )
            }
        // binding.recyclerview.addItemDecoration(decoration) // we can create custom item decorations..
    }

    private fun onItemSelected(superHero: SuperHero) {
        Toast.makeText(this, superHero.publisher.value, Toast.LENGTH_SHORT).show()
    }
}