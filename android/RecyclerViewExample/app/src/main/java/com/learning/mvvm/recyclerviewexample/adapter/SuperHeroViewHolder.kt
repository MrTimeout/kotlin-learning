package com.learning.mvvm.recyclerviewexample.adapter

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.learning.mvvm.recyclerviewexample.SuperHero
import com.learning.mvvm.recyclerviewexample.databinding.ItemSuperheroBinding

class SuperHeroViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemSuperheroBinding.bind(view)

    fun render(superHero: SuperHero, onClickListener: (SuperHero) -> Unit) {
        this.binding.superHeroName.text = superHero.name
        this.binding.superHeroRealName.text = superHero.realName
        this.binding.superHeroPublisher.text = superHero.publisher.value
        Glide.with(this.binding.superHeroImg.context).load(superHero.photo)
            .into(this.binding.superHeroImg)

        this.binding.superHeroImg.setOnClickListener {
            Toast.makeText(
                this.binding.superHeroImg.context,
                superHero.realName,
                Toast.LENGTH_SHORT
            ).show()
        }

        // itemView refers to the super item
        this.itemView.setOnClickListener {
            Toast.makeText(
                this.itemView.context,
                superHero.name,
                Toast.LENGTH_SHORT
            ).show()
        }

        // setOnClickListener using lambdas
        this.binding.superHeroName.setOnClickListener { onClickListener(superHero) }
    }
}