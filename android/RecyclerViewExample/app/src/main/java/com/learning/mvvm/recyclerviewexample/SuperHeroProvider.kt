package com.learning.mvvm.recyclerviewexample

class SuperHeroProvider {
    companion object {
        private const val DEFAULT_NAME = "Spiderman"
        private const val DEFAULT_REAL_NAME = "Peter Parker"
        private const val DEFAULT_URL =
            "https://cdn.pocket-lint.com/r/s/970x/assets/images/159643-tv-news-spider-man-no-way-home-image1-dryautoefj.jpg";
        val superHeroList = (1..15).map {
            SuperHero(
                DEFAULT_NAME + it,
                DEFAULT_REAL_NAME + it,
                PublisherEnum.MARVEL,
                DEFAULT_URL
            )
        }.toList()
    }
}