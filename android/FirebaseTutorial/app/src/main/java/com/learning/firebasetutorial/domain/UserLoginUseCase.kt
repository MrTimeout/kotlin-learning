package com.learning.firebasetutorial.domain

import com.learning.firebasetutorial.data.service.FirebaseAuthService
import com.learning.firebasetutorial.data.model.UserModel
import javax.inject.Inject

class UserLoginUseCase @Inject constructor(private val firebaseAuthService: FirebaseAuthService) {

    suspend operator fun invoke(userModel: UserModel) =
        if (userModel.email.isNotBlank() && userModel.password.isNotBlank()) this.firebaseAuthService.loginUser(
            userModel
        ) else false
}