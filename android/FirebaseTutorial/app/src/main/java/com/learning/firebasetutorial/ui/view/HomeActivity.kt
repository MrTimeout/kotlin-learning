package com.learning.firebasetutorial.ui.view

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.learning.firebasetutorial.databinding.ActivityHomeBinding
import com.learning.firebasetutorial.ui.modelview.HomeViewModel
import com.learning.firebasetutorial.ui.utils.ViewConstants.DISPLAY_ERROR_ON_LOG_OUT
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    private val homeViewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityHomeBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        this.homeViewModel.onStart(this.intent.extras)

        this.homeViewModel.userPreferencesModel.observe(this) {
            this.binding.tvEmailShow.text = it.email
            this.binding.tvProvider.text = it.provider.name
        }

        this.homeViewModel.logOutSuccess.observe(this) {
            if (it) {
                onBackPressedDispatcher.onBackPressed()
            } else {
                showErrorOnLogOut()
            }
        }

        this.binding.btnLogOut.setOnClickListener {
            this.homeViewModel.logOut()
        }
    }

    private fun showErrorOnLogOut() =
        Toast.makeText(this, DISPLAY_ERROR_ON_LOG_OUT, Toast.LENGTH_SHORT).show()
}