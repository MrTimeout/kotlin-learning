package com.learning.firebasetutorial.ui.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.learning.firebasetutorial.data.model.ProviderType
import com.learning.firebasetutorial.databinding.ActivityAuthBinding
import com.learning.firebasetutorial.ui.modelview.AuthViewModel
import com.learning.firebasetutorial.ui.utils.ViewConstants
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthBinding

    private val authViewModel: AuthViewModel by viewModels()

    // registerForActivityResult is safe to call before your fragment or activity is created.
    // You cannot launch the ActivityResultLauncher until the fragment or activity's lifecycle has
    //      reached CREATED.
    // + ActivityResultLauncher takes as parameter: ActivityResultContract and an ActivityResultCallback
    // + ActivityResultContract: It defines the input type needed to produce the result along with
    //      the output type of the result.
    // + ActivityResultCallback: It is a single method interface with an _onActivityResult()_
    //      method that takes an object of the output type defined in the ActivityResultContract
    private val getContent =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
            }
        }

    override fun onStart() {
        super.onStart()

        this.binding.etEmail.isFocused
        this.binding.btnLogin.isClickable = true
        this.binding.btnSignUp.isClickable = true
        this.binding.btnLogInGoogle.isClickable = true
        this.binding.authLayout.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityAuthBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        /*val bundle = Bundle()
        bundle.putString("message", "Integration with firebase completed")

        val firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        firebaseAnalytics.logEvent("InitScreen", bundle)*/

        this.authViewModel.userPreferencesMode.observe(this) {
            this.binding.authLayout.visibility = View.INVISIBLE
            this.showSuccessOnLogin(it.email, it.provider)
        }

        this.authViewModel.loginSuccess.observe(this) {
            if (it) {
                this.showSuccessOnLogin(this.binding.etEmail.text.toString())
            } else {
                this.showErrorOnLogin()
            }
        }

        this.binding.btnSignUp.setOnClickListener {
            it.isClickable = false
            this.authViewModel.signUpUser(
                this.binding.etEmail.text.toString(),
                this.binding.etPassword.text.toString()
            )
            it.isClickable = true
        }

        this.binding.btnLogin.setOnClickListener {
            it.isClickable = false
            this.authViewModel.loginUser(
                this.binding.etEmail.text.toString(),
                this.binding.etPassword.text.toString()
            )
            it.isClickable = true
        }

        this.binding.btnLogInGoogle.setOnClickListener {
            it.isClickable = false

            it.isClickable = true
        }

        this.authViewModel.onStart()
    }

    private fun showSuccessOnLogin(email: String, provider: ProviderType = ProviderType.BASIC) {
        val home = Intent(this, HomeActivity::class.java).apply {
            putExtra(ViewConstants.EMAIL_BUNDLE_PROPERTY, email)
            putExtra(ViewConstants.PROVIDER_BUNDLE_PROPERTY, provider.name)
        }
        this.cleanUpTextFields()
        startActivity(home)
    }

    private fun showErrorOnLogin() {
        Toast.makeText(
            this@AuthActivity,
            ViewConstants.DISPLAY_ERROR_ON_LOG_IN,
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun cleanUpTextFields() {
        this.binding.etEmail.text?.clear()
        this.binding.etPassword.text?.clear()
    }
}