package com.learning.firebasetutorial.domain

import com.learning.firebasetutorial.data.model.UserPreferencesModel
import com.learning.firebasetutorial.data.repository.SharedPreferencesRepository
import javax.inject.Inject

class UserSetPreferencesUseCase @Inject constructor(private val sharedPreferencesRepository: SharedPreferencesRepository) {

    suspend operator fun invoke(userPreferencesModel: UserPreferencesModel): Boolean =
        if (userPreferencesModel.email.isNotBlank()) {
            this.sharedPreferencesRepository.saveUserData(userPreferencesModel)
            true
        } else false
}