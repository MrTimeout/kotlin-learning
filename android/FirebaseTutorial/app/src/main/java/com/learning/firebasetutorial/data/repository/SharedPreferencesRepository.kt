package com.learning.firebasetutorial.data.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.learning.firebasetutorial.data.model.ProviderType
import com.learning.firebasetutorial.data.model.UserPreferencesModel
import kotlinx.coroutines.flow.firstOrNull

class SharedPreferencesRepository constructor(private val context: Context) {
    companion object {
        private const val PREFERENCES_DATABASE_NAME = "settings"
        private const val PREFERENCES_USER_EMAIL_PROPERTY = "email"
        private const val PREFERENCES_USER_PROVIDER_PROPERTY = "provider"

        private val PREFERENCES_USER_EMAIL = stringPreferencesKey(PREFERENCES_USER_EMAIL_PROPERTY)
        private val PREFERENCES_USER_PROVIDER =
            stringPreferencesKey(PREFERENCES_USER_PROVIDER_PROPERTY)
    }

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = PREFERENCES_DATABASE_NAME)

    suspend fun saveUserData(userPreferencesModel: UserPreferencesModel) =
        this.context.dataStore.edit {
            it[PREFERENCES_USER_EMAIL] = userPreferencesModel.email
            it[PREFERENCES_USER_PROVIDER] = userPreferencesModel.provider.name
        }

    suspend fun getUserData(): UserPreferencesModel? =
        this.context.dataStore.data.firstOrNull()?.asMap().let {
            if (((it?.get(PREFERENCES_USER_EMAIL) ?: "") as String).isNotBlank()) {
                return UserPreferencesModel(
                    (it!![PREFERENCES_USER_EMAIL] ?: "") as String,
                    ProviderType.valueOf(
                        (it[PREFERENCES_USER_PROVIDER] ?: ProviderType.BASIC.name) as String
                    ),
                )
            }
            return null
        }

    suspend fun deleteUserData() = this.context.dataStore.edit {
        it.clear()
    }
}