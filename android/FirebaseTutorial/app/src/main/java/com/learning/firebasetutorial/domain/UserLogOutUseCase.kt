package com.learning.firebasetutorial.domain

import com.learning.firebasetutorial.data.service.FirebaseAuthService
import javax.inject.Inject

class UserLogOutUseCase @Inject constructor(private val firebaseAuthService: FirebaseAuthService) {

    operator fun invoke(): Boolean = this.firebaseAuthService.logOutUser()
}