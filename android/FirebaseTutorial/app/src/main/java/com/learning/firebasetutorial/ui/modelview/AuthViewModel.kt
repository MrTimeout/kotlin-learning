package com.learning.firebasetutorial.ui.modelview

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.learning.firebasetutorial.data.model.UserModel
import com.learning.firebasetutorial.data.model.UserPreferencesModel
import com.learning.firebasetutorial.data.repository.SharedPreferencesRepository
import com.learning.firebasetutorial.domain.UserLoginUseCase
import com.learning.firebasetutorial.domain.UserSignUpUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val userSignUpUseCase: UserSignUpUseCase,
    private val userLoginUseCase: UserLoginUseCase,
    private val sharedPreferencesRepository: SharedPreferencesRepository,
) : ViewModel() {

    val loginSuccess = MutableLiveData<Boolean>()

    val userPreferencesMode = MutableLiveData<UserPreferencesModel>()

    fun onStart() {
        viewModelScope.launch {
            val userPreferences = sharedPreferencesRepository.getUserData()
            userPreferences?.run { userPreferencesMode.postValue(this) }
        }
    }

    fun signUpUser(email: String?, password: String?) {
        viewModelScope.launch {
            this@AuthViewModel.loginSuccess.postValue(
                this@AuthViewModel.userSignUpUseCase(
                    UserModel(
                        email ?: "",
                        password ?: ""
                    )
                )
            )
        }
    }

    fun loginUser(email: String?, password: String?) {
        viewModelScope.launch {
            this@AuthViewModel.loginSuccess.postValue(
                this@AuthViewModel.userLoginUseCase(
                    UserModel(
                        email ?: "",
                        password ?: ""
                    )
                )
            )
        }
    }
}