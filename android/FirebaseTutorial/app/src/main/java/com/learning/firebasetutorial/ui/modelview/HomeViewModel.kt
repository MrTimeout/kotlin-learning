package com.learning.firebasetutorial.ui.modelview

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.learning.firebasetutorial.data.model.ProviderType
import com.learning.firebasetutorial.data.model.UserPreferencesModel
import com.learning.firebasetutorial.data.repository.SharedPreferencesRepository
import com.learning.firebasetutorial.domain.UserLogOutUseCase
import com.learning.firebasetutorial.ui.utils.ViewConstants.EMAIL_BUNDLE_PROPERTY
import com.learning.firebasetutorial.ui.utils.ViewConstants.PROVIDER_BUNDLE_PROPERTY
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val userLogOut: UserLogOutUseCase,
    private val sharedPreferencesRepository: SharedPreferencesRepository,
) : ViewModel() {

    val userPreferencesModel = MutableLiveData<UserPreferencesModel>()

    val logOutSuccess = MutableLiveData<Boolean>()

    fun onStart(bundle: Bundle?) = bundle.let {
        viewModelScope.launch {
            val email = it?.getString(EMAIL_BUNDLE_PROPERTY) ?: ""
            // Here we have getParcelable(PROVIDER_BUNDLE_PROPERTY, ProviderType::class.java) but it is necessary to use API 33
            val providerType = ProviderType.valueOf(
                it?.getString(PROVIDER_BUNDLE_PROPERTY) ?: ProviderType.BASIC.name
            )

            val userPreferencesModel = UserPreferencesModel(email, providerType)

            // Check if email is empty
            this@HomeViewModel.sharedPreferencesRepository.saveUserData(userPreferencesModel)
            this@HomeViewModel.userPreferencesModel.postValue(userPreferencesModel)
        }
    }

    fun logOut() {
        viewModelScope.launch {
            if (userLogOut()) {
                sharedPreferencesRepository.deleteUserData()
                logOutSuccess.postValue(true)
            } else {
                logOutSuccess.postValue(false)
            }
        }
    }
}