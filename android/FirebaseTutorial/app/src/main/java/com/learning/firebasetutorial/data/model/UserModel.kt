package com.learning.firebasetutorial.data.model

data class UserModel(
    val email: String,
    val password: String,
    val provider: ProviderType = ProviderType.BASIC,
)

data class UserPreferencesModel(
    val email: String,
    val provider: ProviderType = ProviderType.BASIC,
)

enum class ProviderType {
    BASIC,
    GOOGLE,
    GITHUB
}
