package com.learning.firebasetutorial.ui.utils

object ViewConstants {

    internal const val EMAIL_BUNDLE_PROPERTY = "email"

    internal const val PROVIDER_BUNDLE_PROPERTY = "provider"

    internal const val DISPLAY_ERROR_ON_LOG_IN =
        "An error has occurred while logging in / signing up"

    internal const val DISPLAY_ERROR_ON_LOG_OUT = "An error has occurred while logging out"
}