package com.learning.firebasetutorial.core

import android.content.Context
import com.learning.firebasetutorial.data.repository.SharedPreferencesRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataModule {

    @Singleton
    @Provides
    fun sharedPreferencesRepository(@ApplicationContext context: Context) =
        SharedPreferencesRepository(context)
}