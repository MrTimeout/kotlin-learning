package com.learning.firebasetutorial.domain

import com.learning.firebasetutorial.data.service.FirebaseAuthService
import com.learning.firebasetutorial.data.model.UserModel
import javax.inject.Inject

class UserSignUpUseCase @Inject constructor(private val firebaseAuthService: FirebaseAuthService) {

    suspend operator fun invoke(userModel: UserModel): Boolean =
        if (userModel.email.isNotBlank() && userModel.password.isNotBlank()) firebaseAuthService.signUpUser(
            userModel
        ) else false
}