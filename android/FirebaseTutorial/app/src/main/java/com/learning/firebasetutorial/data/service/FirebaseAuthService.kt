package com.learning.firebasetutorial.data.service

import com.google.firebase.auth.FirebaseAuth
import com.learning.firebasetutorial.data.model.UserModel
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class FirebaseAuthService @Inject constructor(private val firebaseAuth: FirebaseAuth) {

    suspend fun signUpUser(userModel: UserModel): Boolean =
        try {
            this.firebaseAuth.createUserWithEmailAndPassword(
                userModel.email,
                userModel.password
            ).await()
            true
        } catch (e: Exception) {
            false
        }

    suspend fun loginUser(userModel: UserModel): Boolean =
        try {
            this.firebaseAuth.signInWithEmailAndPassword(userModel.email, userModel.password)
                .await()
            true
        } catch (e: Exception) {
            false
        }

    fun logOutUser(): Boolean =
        try {
            this.firebaseAuth.signOut()
            true
        } catch (e: Exception) {
            false
        }
}