package com.learning.mvvm.fragments

interface OnFragmentsActionsListener {

    fun onClickFragmentButton()
}