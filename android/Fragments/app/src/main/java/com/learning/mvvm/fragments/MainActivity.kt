package com.learning.mvvm.fragments

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.learning.mvvm.fragments.databinding.ActivityMain1Binding
import com.learning.mvvm.fragments.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnFragmentsActionsListener {

    private lateinit var binding: ActivityMain1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityMain1Binding.inflate(layoutInflater)
        setContentView(this.binding.root)

        this.binding.btnBlue.setOnClickListener {
            loadFragment(BlueFragment())
        }

        this.binding.btnRed.setOnClickListener {
            loadFragment(RedFragment())
        }
    }

    override fun onClickFragmentButton() {
        Toast.makeText(this, "The button has been clicked", Toast.LENGTH_SHORT).show()
    }

    private fun loadFragment(fragment: Fragment) {
        val fragmentTransaction = this.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.addToBackStack(null) // it allows the user to go back in the screen
        fragmentTransaction.commit()
    }
}