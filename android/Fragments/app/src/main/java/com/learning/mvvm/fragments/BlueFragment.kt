package com.learning.mvvm.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.learning.mvvm.fragments.databinding.FragmentBlueBinding

class BlueFragment : Fragment() {

    private var onFragmentsActionListener: OnFragmentsActionsListener? = null
    private lateinit var fragmentBlueBinding: FragmentBlueBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentsActionsListener) {
            onFragmentsActionListener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.fragmentBlueBinding = FragmentBlueBinding.inflate(inflater, container, false)
        return this.fragmentBlueBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.fragmentBlueBinding = FragmentBlueBinding.bind(view)
        this.fragmentBlueBinding.btnPlus.setOnClickListener {
            this.onFragmentsActionListener?.onClickFragmentButton()
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.onFragmentsActionListener = null
    }
}