package com.learning.mvvm.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.learning.mvvm.fragments.databinding.FragmentRedBinding

class RedFragment : Fragment() {

    private var onFragmentsActionsListener: OnFragmentsActionsListener? = null
    private lateinit var fragmentRedBinding: FragmentRedBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentsActionsListener) {
            this.onFragmentsActionsListener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        this.fragmentRedBinding = FragmentRedBinding.inflate(inflater, container, false)
        return this.fragmentRedBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.fragmentRedBinding.btnPlus.setOnClickListener {
            this.onFragmentsActionsListener?.onClickFragmentButton()
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.onFragmentsActionsListener = null
    }
}