# Fragments

## Flow

1. `onAttach`: Establish a relation between the activity and the fragment.
2. `onCreate`: Creating the fragment
3. `onCreateView`: Here we return the view already created.
4. `onActivityCreated`: Here it is a good place to set listeners of onClick() or initialize
   variables from fragment.
5. `onStart`: When all is ready
6. `onResume`: it is called when the application is minimized and opened again, onStart will not be
   called more than once.