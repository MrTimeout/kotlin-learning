package com.learning.mvvm.examplemvvm.ui.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.learning.mvvm.examplemvvm.databinding.ActivityMainBinding
import com.learning.mvvm.examplemvvm.ui.viewmodel.QuoteViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val quoteViewModel: QuoteViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        this.quoteViewModel.onCreate()

        this.quoteViewModel.isLoading.observe(this) {
            this.binding.progress.isVisible = it
        }

        this.quoteViewModel.quoteModel.observe(this) {
            this.binding.tvQuote.text = it.quote
            this.binding.tvAuthor.text = it.author
        }

        this.binding.viewContainer.setOnClickListener {
            this.quoteViewModel.randomQuote()
        }
    }
}