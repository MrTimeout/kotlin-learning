package com.learning.mvvm.examplemvvm.data

import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import com.learning.mvvm.examplemvvm.data.model.QuoteProvider
import com.learning.mvvm.examplemvvm.data.network.QuoteService
import javax.inject.Inject

class QuoteRepository @Inject constructor(
    private val quoteService: QuoteService,
    private val quoteProvider: QuoteProvider
) {

    suspend fun getQuotes(): List<QuoteModel> {
        val quotes = this.quoteService.fetchQuotes()
        this.quoteProvider.quotes = quotes
        return quotes
    }
}