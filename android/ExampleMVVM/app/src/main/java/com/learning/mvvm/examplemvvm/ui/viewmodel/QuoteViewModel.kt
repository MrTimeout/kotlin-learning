package com.learning.mvvm.examplemvvm.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import com.learning.mvvm.examplemvvm.domain.GetQuotes
import com.learning.mvvm.examplemvvm.domain.GetRandomQuote
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QuoteViewModel @Inject constructor(
    private val getQuotes: GetQuotes,
    private val getRandomQuote: GetRandomQuote,
): ViewModel() {

    val quoteModel = MutableLiveData<QuoteModel>()

    val isLoading = MutableLiveData<Boolean>()

    fun onCreate() {
        viewModelScope.launch {
            this@QuoteViewModel.isLoading.postValue(true)
            val result = this@QuoteViewModel.getQuotes()

            if (result.isNotEmpty()) {
                this@QuoteViewModel.quoteModel.postValue(result[0])
                this@QuoteViewModel.isLoading.postValue(false)
            }
        }
    }

    fun randomQuote() {
        viewModelScope.launch {
            isLoading.postValue(true)
            quoteModel.postValue(getRandomQuote())
            isLoading.postValue(false)
        }
    }
}