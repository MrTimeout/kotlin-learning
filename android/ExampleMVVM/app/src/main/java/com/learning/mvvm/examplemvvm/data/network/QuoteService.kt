package com.learning.mvvm.examplemvvm.data.network

import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class QuoteService @Inject constructor(private val quoteApiService: QuoteApiService) {

    suspend fun fetchQuotes(): List<QuoteModel> {
        return withContext(Dispatchers.IO) {
            this@QuoteService.quoteApiService.fetchQuotes().body() ?: emptyList()
        }
    }
}