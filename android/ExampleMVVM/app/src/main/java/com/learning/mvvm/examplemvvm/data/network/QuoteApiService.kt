package com.learning.mvvm.examplemvvm.data.network

import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import retrofit2.Response
import retrofit2.http.GET

interface QuoteApiService {

    @GET("/.json")
    suspend fun fetchQuotes(): Response<List<QuoteModel>>
}