package com.learning.mvvm.examplemvvm.domain

import com.learning.mvvm.examplemvvm.data.QuoteRepository
import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import javax.inject.Inject

class GetQuotes @Inject constructor(private val quoteRepository: QuoteRepository) {

    suspend operator fun invoke(): List<QuoteModel> = this.quoteRepository.getQuotes()

}