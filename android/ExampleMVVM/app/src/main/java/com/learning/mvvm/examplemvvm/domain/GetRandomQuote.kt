package com.learning.mvvm.examplemvvm.domain

import com.learning.mvvm.examplemvvm.data.QuoteRepository
import com.learning.mvvm.examplemvvm.data.model.QuoteModel
import com.learning.mvvm.examplemvvm.data.model.QuoteProvider
import javax.inject.Inject

class GetRandomQuote @Inject constructor(
    private val quoteRepository: QuoteRepository,
    private val quoteProvider: QuoteProvider
) {

    suspend operator fun invoke(): QuoteModel = quoteRepository.getQuotes().let {
        if (it.isEmpty()) this.quoteProvider.quotes[(this.quoteProvider.quotes.indices).random()] else it[(it.indices).random()]
    }
}