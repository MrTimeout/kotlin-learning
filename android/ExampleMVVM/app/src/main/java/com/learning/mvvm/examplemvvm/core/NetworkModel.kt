package com.learning.mvvm.examplemvvm.core

import com.learning.mvvm.examplemvvm.data.network.QuoteApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModel {

    private const val BASE_URL =
        "https://drawsomething-59328-default-rtdb.europe-west1.firebasedatabase.app/"

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun provideQuotesApiService(retrofit: Retrofit): QuoteApiService =
        retrofit.create(QuoteApiService::class.java)
}