package com.learning.mvvm.uservipsharedpreferences

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.learning.mvvm.uservipsharedpreferences.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        checkUserValues()

        initUI()
    }

    private fun checkUserValues() {
        if (UserVipApplication.prefs.getName()?.isNotBlank() == true) {
            this.goToResultActivity()
        }
    }

    private fun initUI() {
        this.binding.btnContinue.setOnClickListener {
            if (this.binding.etName.text.toString().isNotBlank()) {
                UserVipApplication.prefs.saveName(this.binding.etName.text.toString())
                UserVipApplication.prefs.saveVip(this.binding.cbVip.isChecked)
                this.goToResultActivity()
            } else {
                Toast.makeText(this, "Enter a valid not empty name", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun goToResultActivity() {
        startActivity(Intent(this, ResultActivity::class.java))
    }
}