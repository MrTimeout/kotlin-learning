package com.learning.mvvm.uservipsharedpreferences

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.learning.mvvm.uservipsharedpreferences.databinding.ActivityResultBinding

class ResultActivity : AppCompatActivity() {

    private lateinit var binding: ActivityResultBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityResultBinding.inflate(this.layoutInflater)
        setContentView(this.binding.root)

        this.initUI()
    }

    private fun initUI() {
        this.binding.btnCloseSession.setOnClickListener {
            UserVipApplication.prefs.wipe()
            onBackPressedDispatcher.onBackPressed()
        }
        this.binding.tvWelcome.text = this.welcomeText()
        this.vipBackground()
    }

    private fun welcomeText() = "Welcome ${UserVipApplication.prefs.getName()}"

    private fun vipBackground() {
        if (UserVipApplication.prefs.getVip()) {
            this.binding.container.setBackgroundColor(ContextCompat.getColor(this, R.color.yellow))
        }
    }
}