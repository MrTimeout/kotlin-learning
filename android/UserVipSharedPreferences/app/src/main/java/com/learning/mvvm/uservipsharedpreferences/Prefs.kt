package com.learning.mvvm.uservipsharedpreferences

import android.content.Context
import android.content.SharedPreferences

class Prefs(private val context: Context) {

    companion object {
        private const val SHARED_NAME = "mySharedName"
        private const val SHARED_USER_NAME = "userName"
        private const val SHARED_IS_VIP = "isVip"
    }

    private val storage: SharedPreferences = this.context.getSharedPreferences(SHARED_NAME, 0)

    fun saveName(name: String): Unit = this.storage.edit().putString(SHARED_USER_NAME, name).apply()

    fun saveVip(vip: Boolean): Unit = this.storage.edit().putBoolean(SHARED_IS_VIP, vip).apply()

    fun getName(): String? = this.storage.getString(SHARED_USER_NAME, "")

    fun getVip(): Boolean = this.storage.getBoolean(SHARED_IS_VIP, false)

    fun wipe(): Unit = this.storage.edit().clear().apply()
}