package com.learning.mvvm.uservipsharedpreferences

import android.annotation.SuppressLint
import android.app.Application

class UserVipApplication : Application() {

    companion object {
        // We are only testing, not ready for production
        @SuppressLint("StaticFieldLeak")
        lateinit var prefs: Prefs
    }

    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(this.applicationContext)
    }
}