# Learning Concurrency

## Processes, threads and coroutines

When you start an application, the operating system
will create a process, attach a thread to it, and then start
the execution of that thread - commonly known as the main thread.

### Processes

A process is an instance of an app that is being executed.

### Threads

A thread of execution encompasses a set of instructions for a processor
to execute.

Each thread can access and modify the resources contained in the process
it's attached to, but it also has its own local storage, called _thread-local_ storage.

Android 3.0 and above, for example, will crash an app if a networking operation
is made in the UI thread, in order to discourage developers from doing it.

In Android the main thread is the _UI-Thread_

### Coroutines

Coroutines are "lightweight threads". This is mostly because, like threads, coroutines define
the execution of a set of instructions for a processor to execute.

Only one instruction can be executed in a thread at a given time.

#### Sample

```kotlin
fun coroutineSample() = runBlocking {
    launch {
        delay(1000L)
        println("World!")
    }
    println("Hello")
}
```

- _launch_ is a _coroutine builder_. It launches a new coroutine concurrently with the rest of the
  code, which continues to work independently.
- _delay_ is a special _suspending function_. It _suspends_ the coroutine for a specific time.
  Suspending a coroutine does not block the underlying thread, but allows other coroutines to run
  and use underlying thread for their code.
- _runBlocking_ is also a coroutine builder that bridges the non-coroutine world of a regular fun
  and the code with coroutines inside of `runBlocking`.

If we remove `runBlocking` in this code, we'll get an error on the `launch` call, since `launch` is
declared only in the `CoroutineScope`.

The name of `runBlocking` means that the thread that runs it gets _blocked_ for the duration of the
call, until all the coroutines inside this fun complete their execution.