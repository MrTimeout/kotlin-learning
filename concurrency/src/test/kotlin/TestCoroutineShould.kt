import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.test.Test

internal class TestCoroutineShould {

    @Test
    fun `testing coroutine sample`() {
        println("hey")
        coroutineSample()
        println("bye")
        coroutineSample1()
    }

    @Test
    fun `measuring time to spawn coroutines`() {

    }

    private fun coroutineSample() = runBlocking {
        launch {
            delay(1000L)
            println("World!")
        }
        println("Hello")
    }

    private fun coroutineSample1() = runBlocking {
        launch { doCoroutineSample1Work() }
        println("Hello")
    }

    // Suspending functions can be used inside coroutines just like regular functions, the only
    // difference is that suspend functions can suspend execution of coroutine.
    private suspend fun doCoroutineSample1Work() {
        delay(1000L)
        println("World!")
    }

    internal suspend fun createCoroutines(amount: Int) {
        val jobs = arrayListOf<Job>()
    }
}